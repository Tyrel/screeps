var roleHarvester = {

    /** @param {Creep} creep **/
    run: function(creep) {
        console.log(creep, 'running Harvest');
	    if(creep.carry.energy < creep.carryCapacity) {
	        var source = creep.pos.findClosestByPath(FIND_SOURCES);
            if(creep.harvest(source) == ERR_NOT_IN_RANGE) {
                console.log('\tmoving to', source);
                creep.moveTo(source);
            }
        }
        else {
            var targets = creep.room.find(FIND_STRUCTURES, {
                    filter: (structure) => {
                        return (structure.structureType == STRUCTURE_CONTAINER
                              || structure.structureType == STRUCTURE_EXTENSION 
                              || structure.structureType == STRUCTURE_SPAWN
                              || structure.structureType == STRUCTURE_TOWER
                            ) && structure.energy < structure.energyCapacity;
                    }
            });
            if(targets.length > 0) {
                if(creep.transfer(targets[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    console.log('\tmoving to', targets[0]);
                    creep.moveTo(targets[0]);
                }
            } else {
                creep.moveTo(Game.spawns.Spawn1);
            }
        }
	}
};

module.exports = roleHarvester;



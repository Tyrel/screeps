var helpers = {
    generateName: function(role){
        return role + '_xxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
            return v.toString(16);
        });
    },
    getLastFlag: function(flagName){
       return  _(Object.keys(Game.flags))
            .filter((f) => f.startsWith(flagName))
            .map((f) => f.slice(flagName.length))
            .max();
    },
    findDamagedStructure: function(searcher){
        var targets = searcher.room.find(FIND_STRUCTURES, {
           filter: function(s){
               if(s.id == searcher.id){
                   return false;
               } 
               if (s.hits < s.hitsMax){
                   return true;
               }
               return false;
           }
       });
       return searcher.pos.findClosestByPath(targets);
    }

};

module.exports = helpers;

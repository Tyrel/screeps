

var BreakException= {};

var roleBuilder = {
    run: function(creep){
        if(creep.memory.building && creep.carry.energy == 0){
            creep.memory.building = false;
        }
        if (!creep.memory.building && creep.carry.energy == creep.carryCapacity){
            creep.memory.building = true;
        }
        if (creep.memory.building){
            var targets = creep.room.find(FIND_CONSTRUCTION_SITES);

            var transferErrorCode = creep.build(targets[0]);
            if(transferErrorCode == ERR_NOT_IN_RANGE) {
                creep.moveTo(targets[0]);
            }
        } else {
            var source = creep.pos.findClosestByPath(FIND_SOURCES);
            if(creep.harvest(source) == ERR_NOT_IN_RANGE) {
                console.log('\tmoving to', source);
                creep.moveTo(source);
            }
        }


    }
};
module.exports = roleBuilder;


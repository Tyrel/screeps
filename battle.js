// usernames are all lowercase, we compare with to lower
var USERNAME_WHITELIST = ['chrisinajar', 'ho0ber', 'fractaloop', 'n7-anthony', 'overra', 'tyrel', 'fervens', 'devdaniel'];

module.exports = {
    findEnemy: findEnemy,
    run: run
};

function findEnemy (searcher) {
    var targets = searcher.room.find(FIND_HOSTILE_CREEPS, {
       filter: (c) => {
           return (USERNAME_WHITELIST.indexOf(c.owner.username.toLowerCase()) === -1);
       }
    });
    if (!targets.length) {
        return searcher.room.controller;
    }
    return searcher.pos.findClosestByPath(targets);
}

function run (creep) {
    var target = findEnemy(creep);
    if (!target) {
        target = findWall(creep);
        // getDirectionTo
        if (!creep.pos.isNearTo(target)) {
            return creep.moveTo(target);
        } else {
            creep.attack(target);
        }
        var direction = creep.pos.getDirectionTo(target);
        creep.memory.wallDirection = direction;
    } else {
        creep.moveTo(target);
        creep.attack(target);
    }
}

function findWall (creep) {
    var targets = creep.room.find(FIND_STRUCTURES, {
        filter: function(object) {
            if (object.my) {
                return false;
            }
            if (object.structureType !== STRUCTURE_TOWER && object.structureType !== STRUCTURE_WALL) {
                return false;
            }
           return true;
        }
    });
    return creep.pos.findClosestByPath(targets);
}
